## Structure

```bash
.
├── app  # Contains the main application files.
│   ├── __init__.py
│   ├── geckodriver
│   ├── main.py
│   ├── crud # Contains CRUD database operations.
│   │   ├── __init__.py
│   │   └── app.py
│   ├── models # Contains database models.
│   │   ├── __init__.py
│   │   └── app.py
│   ├── app_types # Contains pydantic types for the app.
│   │   ├── __init__.py
│   │   └── type.py
│   ├── services # Contains services for the app.
│   │   ├── __init__.py
│   │   └── service.py
│   └── config # Contains configuration files.
│       ├── __init__.py
│       ├── webDriver.py
│       └── database.py
├── dashboards # Contains dashboard grafana files.
│   ├── Évolution_de_la_compatibilité_de_l\'application_ChatGPT-1739970861511.json
│   ├── Evolution_de_la_compatibilite_de_l\'application_ChatGPT.png
│   ├── Moyenne_de_compatibilité_par_catégorie-1738867170443.json
│   ├── Moyenne_de_compatibilité_par_catégorie.png
│   ├── Moyenne_des_compatibilités_des applications_populaires_par_pays-1739970806895.json
│   ├── Moyenne_des_compatibilités_des applications_populaires_par_pays.png
│   ├── Répartition_des_applications_par_catégorie-1738867177780.json
│   └── Répartition_des_applications_par_catégorie.png
│   └── Répartition_des_applications_par_compatibilités-1739970691159.json
│   └── Répartition_des_applications_par_compatibilités.png
│   └── Répartition_des_applications_populaires_par_compatibilités-1739970745151.json
│   └── Répartition_des_applications_populaires_en_France_par_compatibilités.png
├── requirements.txt
├── .gitignore
└── README.md
```

## Virtual environment

### Start scrapping

- [Create virtual environment](#create-virtual-environment) (if you haven't already)
- [Activate virtual environment](#activate-virtual-environment)
- [Install dependencies](#install-dependencies)
- ```bash
   cd app/
   python3 main.py
   # and that's it
  ```

### Create virtual environment

```bash
python3 -m venv .env
```

### Activate virtual environment

| Platform | Shell      | Command to activate virtual environment |
| -------- | ---------- | --------------------------------------- |
| POSIX    | bash/zsh   | `$ source <venv>/bin/activate`          |
| POSIX    | fish       | `$ source <venv>/bin/activate.fish`     |
| POSIX    | csh/tcsh   | `$ source <venv>/bin/activate.csh`      |
| POSIX    | PowerShell | `$ <venv>/bin/Activate.ps1`             |
| Windows  | cmd.exe    | `C:\> <venv>\Scripts\activate.bat`      |
| Windows  | PowerShell | `PS C:\> <venv>\Scripts\Activate.ps1`   |

### Deactivate virtual environment

```bash
deactivate
```

### Install dependencies

```bash
pip install -r requirement.txt
```

### Installation de Grafana et Importation d'un Dashboard

## 1. Télécharger l'image et lancer GRAFANA

- Télécharger l'image via docker
```sh
docker pull grafana/grafana
```
- Lancer GRAFANA
```sh
docker run -d --name=grafana -p 3000:3000 grafana/grafana
```

### 2. Accéder à l'Interface
- Ouvrir http://localhost:3000.
- Se connecter avec admin / admin (changer le mot de passe à la première connexion).

### 3. Importer un Dashboard
- Aller dans Dashboards > Import.
- Importer le json.

