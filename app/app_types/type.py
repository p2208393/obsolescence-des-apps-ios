from pydantic import BaseModel
from typing import Optional
from datetime import date

class Categorie(BaseModel):
    id: str
    name: str
    url: Optional[str]

class AppInfo(BaseModel):
    id: str
    name: str
    developer: str
    is_free: Optional[bool]
    url: Optional[str]
    category_id: Optional[str] = None
    rank: Optional[int] = None
    last_release_date: Optional[date] = None

class AppHistory(BaseModel):
    app_id: str
    date: date
    country: str
    category_id: str
    rank: Optional[int]
    ios_version_support: Optional[float]
    is_free: Optional[float]
    last_update_date: Optional[date]
    status: Optional[str] = None
    rating: Optional[int] = None
    nb_rating: Optional[int] = None
    size: Optional[float] = None
    reviews: Optional[int] = None

