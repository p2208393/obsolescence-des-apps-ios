from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from models.models import Base 

#DATABASE_URL = "postgresql://postgres:mysecretpassword@localhost:5432/ios"
DATABASE_URL = "postgresql://postgres:mysecretpassword@localhost:5432/postgres"

engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base.metadata.create_all(bind=engine)
