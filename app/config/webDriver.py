from selenium import webdriver
from selenium.webdriver.firefox.service import Service

def start_webdriver():
    driver_path = "geckodriver"

    options = webdriver.FirefoxOptions()

    # Pour eviter l'ouverture de la fenetre du navigateur
    options.add_argument("--headless")
    
    return webdriver.Firefox(service=Service(driver_path), options=options)

driver = start_webdriver()
