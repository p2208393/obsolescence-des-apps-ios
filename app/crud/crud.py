from models.models import (
    Categorie as CategorieModel,
    AppInfo as AppInfoModel,
    AppHistory as AppHistoryModel,
)
from app_types.type import Categorie, AppInfo, AppHistory
from sqlalchemy.orm import Session
from typing import Optional
from datetime import date


def create_category(db, cat: Categorie) -> CategorieModel | None:
    """
        If categories already exist -> return
    """
    if category_exists(db, cat.id):
        return

    db_category = CategorieModel(id=cat.id, name=cat.name, url=cat.url)
    db.add(db_category)
    db.commit()
    return db_category


def create_app(db, app: AppInfo) -> AppInfoModel | None:
    """
        If app already exist -> update
    """
    app_ = app_exists(db, app.id)

    if app_:
        app_.name = app.name
        app_.developer = app.developer
        app_.category_id = app.category_id
        app_.last_release_date = app.last_release_date
        app_.url = app.url
    else:
        app_ = AppInfoModel(
            id=app.id,
            name=app.name,
            developer=app.developer,
            category_id=app.category_id,
            last_release_date=app.last_release_date,
            url=app.url,
        )
        db.add(app_)
    
    db.commit()
    return app_


def create_app_history(db, app_hist: AppHistory) -> AppHistoryModel | None:
    """
        If app history already exist -> report in 'error_log.txt' and raise exception
    """
    app_history_exist = db.query(AppHistoryModel).filter_by(
        app_id=app_hist.app_id,
        date=app_hist.date,
        country=app_hist.country,
        category_id=app_hist.category_id
    ).first()

    if app_history_exist:
        with open('error_log.txt', 'a') as f:  # Open file in append mode
            f.write(f"""App
                    app_id={app_hist.app_id},
                    date={app_hist.date},
                    country={app_hist.country},
                    category_id={app_hist.category_id}
                    isFree={app_hist.is_free}
                    already exist
                    \n""")
        raise Exception(f"App {app_history_exist.app_id} already exist")
    
    db_history = AppHistoryModel(**app_hist.dict())
    db.add(db_history)
    db.commit()
    return db_history


def category_exists(db, id: str) -> CategorieModel | None:
    return db.get(CategorieModel, id)

def app_exists(db, id: str) -> AppInfoModel | None:
    return db.get(AppInfoModel, id)
