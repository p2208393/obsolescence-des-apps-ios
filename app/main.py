from typing import List
from app_types.type import AppInfo, AppHistory
from config.webDriver import driver
from services.service import (
    get_app_store_top_apps,
    get_categories,
    get_ios_compatibility,
)

import time

from crud.crud import (
    create_category,
    category_exists,
    app_exists,
    create_app,
    create_app_history,
)
from config.connectiondb import SessionLocal

from datetime import datetime

# https://apps.apple.com/fr/charts/iphone
# https://apps.apple.com/fr/charts/iphone/actualit%C3%A9s-apps/6009
# for free add
# ?chart=top-free
# https://apps.apple.com/fr/charts/iphone/actualit%C3%A9s-apps/6009?chart=top-free

# ?chart=top-free
# ?chart=top-paid

apple_base_url = "https://apps.apple.com/"


def main():
    db = SessionLocal()
    countries = ["us", "in", "br", "pl", "fr"]

    for country in countries:
        categories = get_categories(country)
        for categorie in categories:
            create_category(db, categorie)

            base_url = f"{apple_base_url}/{country}/{categorie.url}"
            
            Apps = get_app_store_top_apps(base_url, is_free=False)
            Apps += get_app_store_top_apps(base_url, is_free=True)

            for app in Apps:
                
                app.category_id = categorie.id
                create_app(db, app)
                
                print(f"Categorie: {categorie.name}")
                print(f"Name: {app.name}")
                print(f"Rank: {app.rank}")
                print(f"Developer: {app.developer}")
                print(f"URL: {app.url}")
                print(f"isFree: {app.is_free}")
                
                try:
                    app_url = app.url.split(f"/{country}/")[1]
                    ios_compatibility = get_ios_compatibility(
                        f"https://apps.apple.com/{country}/{app_url}"
                    )
                    print("ios compatibility", ios_compatibility)
                    app_hist = AppHistory(
                        app_id=app.id,
                        date=datetime.now().date(),
                        country=country,
                        category_id=categorie.id,
                        rank=app.rank,
                        ios_version_support=ios_compatibility,
                        is_free=app.is_free,
                        last_update_date=datetime.now().date()
                    )
                    create_app_history(db, app_hist)
                except Exception as e:
                    print(e)

                time.sleep(0.03)
                print("\n")

    print("Done")
    driver.quit()
    db.close()


if __name__ == "__main__":
    main()
