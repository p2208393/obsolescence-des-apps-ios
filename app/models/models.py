from pydantic import BaseModel
from typing import Optional
from datetime import date
from sqlalchemy import Column, Integer, String, Date, Float, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

class Categorie(Base):
    __tablename__ = 'Categories'

    id = Column(String, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    url = Column(String, nullable=False)
    #for https://apps.apple.com/fr/charts/iphone/shopping-apps/6024 url = /charts/iphone/shopping-apps/6024

    applications = relationship("AppInfo", back_populates="category")


class AppInfo(Base):
    __tablename__ = 'Applications'

    id = Column(String, primary_key=True)
    name = Column(String, nullable=False)
    developer = Column(String, nullable=False)
    category_id = Column(String, ForeignKey('Categories.id'), nullable=False)
    last_release_date = Column(Date, nullable=True)
    url = Column(String, nullable=False)

    category = relationship("Categorie", back_populates="applications")

    history = relationship("AppHistory", back_populates="app")


class AppHistory(Base):
    __tablename__ = 'AppHistory'

    app_id = Column(String, ForeignKey('Applications.id'), primary_key=True)
    date = Column(Date, primary_key=True)
    country = Column(String, primary_key=True)
    category_id = Column(String, ForeignKey('Categories.id'), primary_key=True)
    rank = Column(Integer, nullable=True)
    ios_version_support = Column(Float, nullable=True)
    is_free = Column(Boolean, nullable=True)
    last_update_date = Column(Date, nullable=True)
    status = Column(String, nullable=True)
    rating = Column(Integer, nullable=True)
    nb_rating = Column(Integer, nullable=True)
    size = Column(Float, nullable=True)
    reviews = Column(Integer, nullable=True)

    app = relationship("AppInfo", back_populates="history")
