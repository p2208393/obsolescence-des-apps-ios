from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException

from typing import List
from app_types.type import AppInfo, Categorie
from config.webDriver import driver, start_webdriver

# "https://apps.apple.com/fr/charts/iphone/top-free-apps/36"

"""
Fetches the top apps from the Apple App Store for a given URL.
Args:
    url (str): The URL of the Apple App Store page to scrape.
Returns:
    List[AppInfo]: A list of AppInfo objects containing information about the top free apps.
"""


def get_app_store_top_apps(url: str, is_free: bool) -> List[AppInfo]:
    global driver

    url = f"{url}{'?chart=top-free' if is_free else '?chart=top-paid'}"
    List_of_apps: List[AppInfo] = []
    try:
        if driver.service.process is None:  # Vérifier si le WebDriver est mort
            print("WebDriver n'est plus actif. Redémarrage...")
            driver.quit()
            driver = start_webdriver()
            
        driver.get(url)
        list_of_apps = driver.find_elements(
            By.CSS_SELECTOR,
            ".we-lockup",
        )
        for app in list_of_apps:
            rank = app.find_element(By.CSS_SELECTOR, ".we-lockup__rank").text
            name = app.find_element(By.CSS_SELECTOR, ".we-lockup__title").text
            developer = app.find_element(By.CSS_SELECTOR, ".we-lockup__subtitle").text
            url = app.get_attribute("href")
            id = url.split("/")[-1]
            appInfo = AppInfo(
                id=id, name=name, developer=developer, url=url, rank=int(rank), is_free=is_free
            )
            List_of_apps.append(appInfo)

        return List_of_apps
    except Exception as e:
        print(e)
        return []


"""
Fetches the version history of an app from its Apple App Store page.
Args:
    url (str): The URL of the app's Apple App Store page.
Returns:
    List[str]: A list of version history strings.
"""


def get_version_history(url) -> List[str]:
    driver.get(url)
    try:

        # Wait for the button to be present and clickable
        version_history_button = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//button[text()="Historique des mises à jour"]')
            )
        )
        version_history_button.click()

        # Wait for the list of versions to be present
        list_of_versions = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located((By.XPATH, "//h4"))
        )

        return [version.text for version in list_of_versions]

    except TimeoutException:
        print("Element not found or not clickable within the given time")
    except Exception as e:
        print(e)


"""
Fetches IOS compatibility of an app from its Apple App Store page.
Args:
    url (str): The URL of the app's Apple App Store page.
    Returns:
    List[str]: A list of IOS compatibility strings.
"""


def get_ios_compatibility(url) -> int | None:
    global driver
    
    try:
        if driver.service.process is None:  # Vérifier si le WebDriver est mort
            print("WebDriver n'est plus actif. Redémarrage...")
            driver.quit()
            driver = start_webdriver()
        
        driver.get(url)
        # Wait for the button to be present and clickable
        ios_compatibility = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located(
                (
                    By.XPATH,
                    "//dl[@class='information-list__item__definition__item']",
                )
            )
        )

        for item in ios_compatibility:
            dt_element = item.find_element(By.XPATH, "./dt")
            if dt_element.text.strip() == "iPhone":
                dd_element = item.find_element(By.XPATH, "./dd")
                text_split = dd_element.text.split(" ")
                for i in range(0, len(text_split)):
                    if text_split[i] == "iOS":
                        return text_split[i + 1]

        raise Exception(f"IOS Compatibility not found for {url}")

    except TimeoutException:
        print("Element not found or not clickable within the given time")
        return None
    except Exception as e:
        print(e)
        return None


"""
Fetches the list of app categories from the Apple App Store.
Args:
    country(str): "fr" | "us" | ...
Returns:
    List[Categorie]: A list of Categorie objects containing information about app categories.
"""


def get_categories(country: str) -> List[Categorie]:
    url = f"https://apps.apple.com/{country}/charts/iphone"
    global driver
    try:
        if driver.service.process is None:  # Vérifier si le WebDriver est mort
            print("WebDriver n'est plus actif. Redémarrage...")
            driver.quit()
            driver = start_webdriver()
            
        driver.get(url)
        btn_categories = WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, '//button[@data-we-charts-filter-dropdown-trigger="apps"]')
            )
        )
        btn_categories.click()

        wlist_categories = WebDriverWait(driver, 10).until(
            EC.presence_of_all_elements_located(
                (By.XPATH, '//a[@class="we-genre-filter__item"]')
            )
        )

        return [
            Categorie(
                id=cat.get_attribute("href").split("/")[-1],
                name=cat.text,
                url=cat.get_attribute("href").split(f"/{country}/")[1],
            )
            for cat in wlist_categories
        ]

    except TimeoutException:
        print("Element not found or not clickable within the given time")
        return []
    except Exception as e:
        print(f"Categorie {country} error {e}")
        return []
